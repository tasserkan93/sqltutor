<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Ixudra\Curl\Facades\Curl;

class TeachController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(session()->get(DFN_USER)['userType']!=1)
        return 'Bu sayfaya erişim izniniz bulunmamaktadır';
        $this->response  = Curl::to(DFN_BASE_URL.'titles/all')
            ->get();
        $allTitles =json_decode($this->response ,true)[DFN_RSLT];
        session()->put('titles',$allTitles);
        
        $this->response  = Curl::to(DFN_BASE_URL.'vclass/findAll/'.session()->get(DFN_USER)['userId'])
            ->get();
           $classes= json_decode(  $this->response,true)[DFN_RSLT];
       return view('pages.teachindex',compact('classes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function subject($title,$id,$difficult)
    {
        $this->response = Curl::to(DFN_BASE_URL.'learn/level/'.$id)
            ->withHeader('auth:'.session()->get(DFN_USER)['token'] )
            ->asJson()
            ->get();

        $data = json_decode( json_encode($this->response),true);
        if (!$data[DFN_STATUS])
            return $data['message'];
        $data = json_decode( json_encode($this->response),true)[DFN_RSLT];
        $answerId= $data['questions'][$difficult]['id'];
        $answer = $data['questions'][$difficult]['answer'];
        $this->response = Curl::to(DFN_BASE_URL.'learn/answer/'.$answerId)
            ->withHeader('auth:'.session()->get(DFN_USER)['token'] )
            ->withContentType('application/json')
            ->withData( array( 'query' =>$answer ) )
            ->asJson()
            ->post();
        $answer=  json_decode (json_encode ($this->response),true);

        return view(DFN_VIEW_PAGES.'teachsubjects',compact('title','id','data','difficult','answer'));
    }

    public function create()
    {
        if(session()->get(DFN_USER)['userType']!=1)
        return 'Bu sayfaya erişim izniniz bulunmamaktadır';
        return view('pages.createclass');
    }
    public function createClass(Request $request)
    {


        $this->response = Curl::to(DFN_BASE_URL.'vclass/create')
            ->withHeader('auth:'.session()->get(DFN_USER)['token'] )
            ->withContentType('application/json')
            ->withData( array( 'name' =>$request->classname,'generatorId'=>session()->get(DFN_USER)['userId'] ) )
            ->asJson()
            ->post();
            $data = json_decode(json_encode($this->response),true);
            if(isset($data['message']))
            {
                return back()->with('messages',$data['message']);    
            }
            
    }

    public function detay($classId)
    {
        
        
        $this->response  = Curl::to(DFN_BASE_URL.'vclass/details/'.$classId)
            ->get();

           $data= json_decode($this->response,true)[DFN_RSLT];

           $this->response  = Curl::to(DFN_BASE_URL.'user/all')
            ->get();

            $users= json_decode($this->response,true)[DFN_RSLT];
            return view('pages.classdetails',compact('data','classId','users'));
    }

    public function myclass()
    {
        
        
        $this->response  = Curl::to(DFN_BASE_URL.'vclass/findAll/'.session()->get(DFN_USER)['userId'])
        ->get();
       $classes= json_decode(  $this->response,true)[DFN_RSLT];
   return view('pages.myclass',compact('classes'));
    }

    public function addUser($classId,$userId)
    {
       
            $this->response  = Curl::to(DFN_BASE_URL.'vclass/add')
            ->withHeader('auth:'.session()->get(DFN_USER)['token'] )
            ->withContentType('application/json')
            ->withData( array( 'userId' =>$userId,'classId'=>$classId ) )
            ->asJson()
            ->post();
            $message=json_decode(json_encode($this->response),true)['message'];

              return back()->with('message',$message) ;
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
