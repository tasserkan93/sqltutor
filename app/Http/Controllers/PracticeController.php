<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Ixudra\Curl\Facades\Curl;


class PracticeController extends Controller
{
    private $response;
    public  function  index($title="",$id="",$difficult="")
    {
        if (!session()->has(DFN_USER))
            return redirect('/login');
        $this->response = Curl::to(DFN_BASE_URL.'learn/level/'.$id)
            ->withHeader('auth:'.session()->get(DFN_USER)['token'] )
            ->asJson()
            ->get();

           $data = json_decode( json_encode($this->response),true);
           if (!$data[DFN_STATUS])
           return $data['message'];
        $data = json_decode( json_encode($this->response),true)[DFN_RSLT];
        return view(DFN_VIEW_PAGES.'practice',compact('title','id','data','difficult'));
    }

    public function executeQuery(Request $request)
    {
        $this->response = Curl::to(DFN_BASE_URL.'learn/answer/'.$request->questionId)
            ->withHeader('auth:'.session()->get(DFN_USER)['token'] )
            ->withContentType('application/json')
            ->withData( array( 'query' => $request->answer ) )
            ->asJson()
            ->post();
            $this->response=  json_decode (json_encode ($this->response),true);
            if(!$this->response[DFN_STATUS])
                return redirect('/');

        return $this->response;
    }
    
}
