<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Ixudra\Curl\Facades\Curl;


class LoginController extends Controller
{

    private  $response;
    public  function index()
    {
        if (session()->has(DFN_USER))
            return redirect('/');

        return view(DFN_VIEW_PAGES.'login');
    }


    public function  create(Request $request)
    {
        
        if(strlen($request->password)<6)
        return back()->with('error','Şifre 6 haneden küçük olamaz');
        if($request->password!=$request->password_confirm)
        return back()->with('error','Parolalar uyuşmuyor');
        $this->response = Curl::to(DFN_BASE_URL.'user/register')
        ->withData( 
            [ DFN_USR_NAME => $request->username , 
              DFN_USR_PASS =>$request->password,
              DFN_NAME  => $request->name,
              DFN_USR_MAIL => $request->email,
              DFN_USR_TYPE => $request->userType] )
        ->asJson()
        ->post();

        return  json_decode(json_encode ($this->response),true);
    }
    public  function  login(Request $request)
    {
        if (session()->has(DFN_USR_ID))
            return redirect('/');

        
        $this->response = Curl::to(DFN_BASE_URL.'user/login')
            ->withData( array( DFN_USR_NAME => $request->username , DFN_USR_PASS =>$request->password ) )
            ->asJson()
            ->post();
        $user =  json_decode(json_encode($this->response),true)[DFN_RSLT];
        if ($user)
        {
            $this->response  = Curl::to(DFN_BASE_URL.'titles/all')
                ->get();
            $allTitles =json_decode($this->response ,true)[DFN_RSLT];
            session()->put(DFN_ALL_TITLES,$allTitles);
            session()->put(DFN_USER,$user);
            $this->response  = Curl::to(DFN_BASE_URL.'titles/title/'.$user['levelId'])
                ->get();
            $title = json_decode($this->response ,true)[DFN_RSLT];
            if(session()->get(DFN_USER)['userType']==1)
            return redirect('/teacher');
            
            return  redirect('/'.$title[0]['title'].'/'.$user['levelId'].'/'.$user['difficulty']);
        }

        return back()->with('status','Kullanıcı adı yada şifreniz yanlış');

    }

}
