<?php
define('DFN_USR_ID'     , 'userId');
define('DFN_RSLT'       , 'result');
define('DFN_BASE_URL'   , 'http://localhost:8084/SqlTutor/api/');
define('DFN_ALL_TITLES'   , 'alltitles');
define('DFN_VIEW_PAGES'   , 'pages.');
define('DFN_USER'   , 'user');
define('DFN_USR_NAME','username');
define('DFN_NAME','name');
define('DFN_USR_MAIL','email');
define('DFN_USR_TYPE','userType');
define('DFN_USR_PASS','password');
define('DFN_DEF','definition');
define('DFN_QUEST','questions');
define('DFN_STATUS', 'status' );