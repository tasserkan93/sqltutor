<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
require  'defines.php';
class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function __construct()
    {
        date_default_timezone_set('Europe/Istanbul');
    }
    public  function  index()
    {
        if (!session()->has(DFN_USER))
            return redirect('/login');

        if(session()->get(DFN_USER)['userType']!=0)
            return 'Bu alanı kullanma yetkiniz bulunmamaktadır';
        return view('pages.index');
    }
    public function profile()
    {
      return view('pages.profile');
    }
}
