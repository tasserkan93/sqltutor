$('#queryForm').on('submit',function(e){
   e.preventDefault();
   var data = $(this).serializeArray();
    console.log($(this).serializeArray());
    
    $.ajax({
        type : 'post',
        url : '/execquery',
        data : data,
        success : function(result)
        {
        
            if(result!=undefined&&result!=null)
            {
                console.log(result);
                
               if(!result.result.correct)
               {
                   warnings='';
                   if(result.result.message!=null)
                   {
                       if(result.result.warnings!=null)
                       {
                        for (var i = 0; i < result.result.warnings.length; i++) {
                            warnings+='<br>'+result.result.warnings[i];
                        }
                       }
                    notifications('error',result.result.message+warnings,'top-right');
                    $('#resultTable').html('');
                   }
                   if(result.result.tblResult!=null)
                   {
                        notifications('warning',result.result.message,'top-right');
                      fillTable(result.result.tblResult,result.result.columnNames);
                   }
                
               
               }
               else if(result.result.correct)
               {
                    notifications('success',result.result.message,'top-right');
                    if(result.result.tblResult!=null)
                    {
                       fillTable(result.result.tblResult,result.result.columnNames);  
                    }
               }
                
            }
              
                
        }
    });
});


function notifications(context,message,position)
{

        toastr.options.timeOut = "false";
        toastr.options.closeButton = true;

            $context = context;
            $message = message;
            $position = position;

            if($context == '') {
                $context = 'info';
            }

            if($position == '') {
                $positionClass = 'toast-left-top';
            } else {
                $positionClass = 'toast-' + $position;
            }

            toastr.remove();
            toastr[$context]($message, '' , { positionClass: $positionClass });
      

        $('#toastr-callback1').on('click', function() {
            $message = $(this).data('message');

            toastr.options = {
                "timeOut": "300",
                "onShown": function() { alert('onShown callback'); },
                "onHidden": function() { alert('onHidden callback'); }
            }

            toastr['info']($message);
        });

        $('#toastr-callback2').on('click', function() {
            $message = $(this).data('message');

            toastr.options = {
                "timeOut": "10000",
                "onclick": function() { alert('onclick callback'); },
            }

            toastr['info']($message);

        });

        $('#toastr-callback3').on('click', function() {
            $message = $(this).data('message');

            toastr.options = {
                "timeOut": "10000",
                "closeButton": true,
                "onCloseClick": function() { alert('onCloseClick callback'); }
            }

            toastr['info']($message);
        });
    
}

function fillTable(table,columns)
{
    var th = '';
    var td = '';
     for(var i=0;i<columns.length;i++){
         th+='<th>'+columns[i]+'</th>';
     }   
    
     for (var i = 0; i < table.length; i++) {
        td +='<tr>';
        for (let j = 0; j < columns.length; j++) {
            td+='<td>'+table[i][j]+'</td>';
        }
        td+='</tr>'
     }
     var table = '<div class="panel panel-headline"><div class="panel-body"> <table class="table table-bordered">'+th+td+'</table></div></div>'
     $('#resultTable').html(table);
 
}