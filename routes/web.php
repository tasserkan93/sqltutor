<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','Controller@index');

Route::get('/login', 'LoginController@index');
Route::post('/register', 'LoginController@create');

Route::get('/register', function () {
    return view('pages.register');
});

Route::get('/{title}/{id}/{difficult}','PracticeController@index');


Route::post('/execquery','PracticeController@executeQuery');

Route::post('/login','LoginController@login');

Route::get('/teach/{title}/{id}/{difficult}','TeachController@subject');
Route::get('/teacher', 'TeachController@index');
Route::get('/createclass', 'TeachController@create');
Route::get('/myclasses', 'TeachController@myClass');
Route::post('/createclass', 'TeachController@createClass');
Route::get('/class/add/{cId}/{uId}','TeachController@addUser');
Route::get('/class/{id}','TeachController@detay');
Route::get('/user/{id}','Controller@profile');

Route::get('/logout', function () {
    session()->forget('user');
    session()->flush();
    return redirect('/login');
});