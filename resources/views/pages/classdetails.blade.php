@extends('master.teach')
@section('content')
    <div class="panel panel-headline">
        <div class="panel-heading">
            <h3 class="panel-title">Sınıf Detayı</h3>
           <p style="font-size:18px"> <span class="fa fa-warning" style="color:orange"></span> Sınıf detayları öğrencilerinizin durumunu takip etmenize yardımcı olur. Aşağıda bulu
           nan tablodan öğrenci takibi yapabilirsiniz yada sınıfınıza öğrenci ekleyebilirsiniz</p>
        </div>
        <div class="panel-body">
         
            <div class="row">
                <div class="col-md-12">
                    <!-- REALTIME CHART -->
                    <div class="panel">
                        <div class="panel-heading">
                            <h3 class="panel-title">Sınıflar Tablosu</h3>

                        </div>
                        <div class="panel-body">
                        <div class="col-md-8">
                        
                            <table class="table table-bordered">
                                <tr>
                                   <th>#</th>
                                    <th>Sınıf Adı</th>
                                    <th>Öğrenci Adı</th>
                                    <th>Durumu</th>
                                    <th>İşlemler</th>
                                </tr>
                                    @php( $count=1)
                                    @if(isset($data))
                                        @foreach($data as $class)
                                       
                                        <tr>
                                        <td>{{$count++}}</td>
                                            <td>{{ ucfirst($class['className'])}}</td>
                                            <td>{{ ucfirst($class['userName'])}}</td>
                                            <td>{{ $class['level']}} Level in {{$class['difficulty']}}. seviyesinde</td>                                        
                                            <td>
                                            <a  href="{{DFN_BASE_URL}}/vclass/details/{{$class['userId']}}"
                                            class="btn btn-danger">Sınıftan Çıkar</a></td>
                                         </tr>
                                        @endforeach
                                    @endif
                               
                            </table>
                            </div>
                            <div class="col-md-4">
                            <table class="table table-bordered">
                                <tr>
                                   
                                    <th>Öğrenci Adı</th>
                                    <th></th>
                                </tr>
                                   
                                    @if(isset($users))
                                        @foreach($users as $user)
                                        <tr>
                                            <td>{{ ucfirst($user['name'])}}</td>
                                                                                                                         
                                            <td>
                                            <a  href="/class/add/{{$classId}}/{{$user['userId']}}"
                                            class="btn btn-success">Ekle</a></td>
                                         </tr>
                                        @endforeach
                                    @endif
                               
                            </table>
                            </div>
                            
                           
                        </div>
                        
                        @if(session('message'))
                                <div class="alert alert-warning">
                                    {{session('message')}}
                                </div>
                         
                        @endif
                    </div>
                    <!-- END REALTIME CHART -->
                </div>

            </div>
        </div>
    </div>
@endsection

