<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>SQL 'ci Kayıt Ekranı</title>
    <link href="http://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
    <script src="http://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<style>
    img {width:100%;}   
</style>
</head>
<body>
    <section class="testimonial py-5" id="testimonial">
        <div class="container">
            <div class="row ">
                <div class="col-md-4 py-5 bg-primary text-white text-center ">
                    <div class=" ">
                        <div class="card-body">
                            <img src="http://www.ansonika.com/mavia/img/registration_bg.svg" style="width:30%">
                            <h2 class="py-3">Kayıt İşlemi</h2>
                            <p>
                                SQL 'ci sistemimize hem öğrenci hemde öğretmen olarak kayıt olabilirsiniz.
                                Kayıt formunu doldurarak sistemize hemen üye olun.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-md-8 py-5 border">
                    <h4 class="pb-4"> SQL 'ci Kayıt Formu</h4>
                    <form method="post" action="register">
                        {{csrf_field()}}
                        <div class="form-row">
                            <div class="form-group col-md-6">
                              <input id="Full Name" name="name" placeholder="Ad Soyad" class="form-control"
                               type="text" required>
                            </div>
                            <div class="form-group col-md-6">
                              <input type="email" class="form-control" id="inputEmail4" name="email" 
                              placeholder="Email" required>
                            </div>
                          </div>
                          <div class="form-row">
                                <div class="form-group col-md-6">
                                        <input  name="username" placeholder="Kullanıcı Adı" class="form-control" 
                                        type="text" required>
                                      </div>
                            <div class="form-group col-md-6">
                                      
                                      <select id="inputState" class="form-control" name="userType">
                                        <option selected>Üyelik türü</option>
                                        <option value="1"> Öğretmen</option>
                                        <option value="0"> Öğrenci</option>
                                      </select>
                            </div>
                           
                        </div>
                          <div class="form-row">
                            <div class="form-group col-md-6">
                              <input id="password" name="password" 
                              placeholder="Şifre" class="form-control" type="password">
                            </div>
                            <div class="form-group col-md-6">
                              <input type="password" class="form-control" id="password" 
                              placeholder="Şifre Doğrula" name="password_confirm">
                            </div>
                          </div>
                       
                       
                     
                        <div class="form-row">
                            <button type="submit" class="btn btn-danger">Kaydol</button>
                            <a href="/login" style="margin-left:10px;margin-top:8px">Zaten üyeliğim var</a>
                        </div>
                    </form>
                    <br>                    
                    @if(session('error'))
                                <div class="alert alert-danger">
                                    {{session('error')}}
                                </div>

                            @endif
                </div>
            </div>
        </div>
    </section>
    
</body>
</html>
