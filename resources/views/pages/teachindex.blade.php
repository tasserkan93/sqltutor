@extends('master.teach')
@section('content')
    <div class="panel panel-headline">
        <div class="panel-heading">
            <h3 class="panel-title">Anasayfa</h3>
            <p class="panel-subtitle">En son giriş : {{date('d.m.Y H:i:s')}}</p>
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-md-6">
                    <div class="metric">
                        <span class="icon"><i class="lnr lnr-construction"></i></span>
                        <p>
                            <span class="number">5</span>
                            <span class="title">SINIFLARIM</span>
                        </p>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="metric">
                        <span class="icon"><i class="fa fa-users"></i></span>
                        <p>
                            <span class="number">10</span>
                            <span class="title">ÖĞRENCİLERİM</span>
                        </p>
                    </div>
                </div>


            </div>
            <div class="row">
                <div class="col-md-12">
                    <!-- REALTIME CHART -->
                    <div class="panel">
                        <div class="panel-heading">
                            <h3 class="panel-title">Sınıflar Tablosu</h3>

                        </div>
                        <div class="panel-body">
                            <table class="table table-bordered">
                                <tr>
                                   <th>#</th>
                                    <th>Sınıf Adı</th>
                                    <th>Sınıf Mevcudu</th>
                                    <th>Sınıf Hocası</th>
                                    <th>Oluşturulma Tarihi</th>
                                </tr>
                                    @php( $count=1)
                                    @if(isset($classes))
                                        @foreach($classes as $class)
                                        <tr>
                                        <td>{{$count++}}</td>
                                            <td>{{ ucfirst($class['name'])}}</td>
                                            <td>{{ ucfirst($class['generatorId'])}}</td>
                                            <td>{{ session()->get(DFN_USER)['name']}}</td>
                                            <td>{{ date('d.m.Y',strtotime(explode(' ',$class['createdDate'])[0])) }}</td>
                                            <td><a href="/class/{{$class['id']}}">Detay</a></td>
                                         </tr>
                                        @endforeach
                                    @endif
                               
                            </table>
                        </div>
                    </div>
                    <!-- END REALTIME CHART -->
                </div>

            </div>
        </div>
    </div>
@endsection

