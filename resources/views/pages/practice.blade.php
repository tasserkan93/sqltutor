@extends('master.home')

@section('title')
    SQL'ci | {{$title}} Uygulama
    @endsection
@section('link')
<link rel="stylesheet" href="{{asset('assets/vendor/toastr/toastr.min.css')}}">
    <style>
        .switch {
            position: relative;
            display: inline-block;
            width: 60px;
            height: 30px;
        }

        /* Hide default HTML checkbox */
        .switch input {
            display: none;
        }

        /* The slider */
        .slider {
            position: absolute;
            cursor: pointer;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            background-color: #ccc;
            -webkit-transition: .4s;
            transition: .4s;
        }

        .slider:before {
            position: absolute;
            content: "";
            height: 26px;
            width: 26px;
            left: 4px;
            bottom: 2px;
            background-color: white;
            -webkit-transition: .4s;
            transition: .4s;
        }

        input:checked + .slider {
            background-color: #2196F3;
        }

        input:focus + .slider {
            box-shadow: 0 0 1px #2196F3;
        }

        input:checked + .slider:before {
            -webkit-transform: translateX(26px);
            -ms-transform: translateX(26px);
            transform: translateX(26px);
        }

        /* Rounded sliders */
        .slider.round {
            border-radius: 34px;
        }

        .slider.round:before {
            border-radius: 50%;
        }
        #uyari{
            display: none;
        }

    </style>
@endsection

@section('content')
    <div class="panel panel-headline">
        <div class="panel-heading">
            <span>
                    <ul class="list-inline text-center">
                        <li class="panel-title" style="float: left">{{$title}}</li>
                        <li style="float: left">

                            <label class="switch" >
                             <input type="checkbox" id="switchBtn">
                         <span class="slider round"></span>
                            </label>
                        </li>

                         <li><a href="/{{$title}}/{{$id}}/0" title="Kolay Seviye"> <i class="@if($difficult==0) fa fa-star
                            @else fa fa-star-o @endif" style="font-size: 25px;  color: green  "></i></a></li>

                        <li> <a href="/{{$title}}/{{$id}}/1" title="Orta Seviye">
                            <i class="@if($difficult==1)fa fa-star
                            @else fa fa-star-o  @endif" style="font-size: 25px;  color: orange  "></i>  </a></li>
                        <li>
                            <a href="/{{$title}}/{{$id}}/2" title="Zor Seviye"> <i class="@if($difficult==2) fa fa-star
                              @else fa fa-star-o @endif"  style="font-size:25px;  color: red  "></i>
                             </a>
                        </li>
                    </ul>
            </span>
            <div class="alert alert-info alert-dismissible" role="alert" id="uyari">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                            aria-hidden="true">×</span></button>
                <i class="fa fa-info-circle"></i> {{$data['level'][DFN_DEF]}}
            </div>
        </div>
        <div class="panel-body" style="margin-top: -20px">
            <div class="row">
                <form action="/execquery" method="post" id="queryForm">
                {{csrf_field()}}
                <span style="font-size: 20px;font-weight: bold;margin-left: 10px">Soru : </span>
                <span style="font-size: 18px;">{{ ucfirst( $data[DFN_QUEST][$difficult]['question'])}} </span>
                <input type="hidden" name="questionId" value="{{$data[DFN_QUEST][$difficult]['id']}}">
                <textarea id="" class="form-control" cols="10" rows="6"
                          placeholder="Sorgunuzu yazınız" name="answer" required></textarea>
                <button type="submit" class="btn btn-info btn-block btn-lg" style="margin: 5px 5px 5px 3px">Sorgula
                </button>
                </form>
            </div>
        </div>
    
    </div>

            <div class="row" id="resultTable"></div>
    

@endsection

@section('script')
<script src="{{asset('assets/vendor/toastr/toastr.min.js')}}"></script>
<script src="{{asset('/js/exec.js')}}"></script>
    <script>
        $(function () {
            $('#switchBtn').click(function () {
                $('#uyari').toggle();
            });
        });
    </script>
@endsection