@extends('master.teach')
@section('content')
    <div class="panel panel-headline">
        <div class="panel-heading">
            <h3 class="panel-title">Sınıflarım</h3>
            
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-md-12">
                    <!-- REALTIME CHART -->
                    <div class="panel">
                        <div class="panel-heading">
                            <h3 class="panel-title">Sınıflar Tablosu</h3>

                        </div>
                        <div class="panel-body">
                            <table class="table table-bordered">
                                <tr>
                                   <th>#</th>
                                    <th>Sınıf Adı</th>
                                    <th>Sınıf Mevcudu</th>
                                    <th>Sınıf Hocası</th>
                                    <th>Oluşturulma Tarihi</th>
                                </tr>
                                    @php( $count=1)
                                    @if(isset($classes))
                                        @foreach($classes as $class)
                                        <tr>
                                        <td>{{$count++}}</td>
                                            <td>{{ ucfirst($class['name'])}}</td>
                                            <td>{{ ucfirst($class['generatorId'])}}</td>
                                            <td>{{ session()->get(DFN_USER)['name']}}</td>
                                            <td>{{ date('d.m.Y',strtotime(explode(' ',$class['createdDate'])[0])) }}</td>
                                            <td><a href="/class/{{$class['id']}}">Detay</a></td>
                                            <td><a href="/class/{{$class['id']}}" class="btn btn-danger">Sil</a></td>
                                         </tr>
                                        @endforeach
                                    @endif
                               
                            </table>
                        </div>
                    </div>
                    <!-- END REALTIME CHART -->
                </div>

            </div>
        </div>
    </div>
@endsection

