@extends('master.home')
@section('title')
@section('link')
    <style>
        .easy-pie-chart#system-load .percent,.easy-pie-chart#system-load1 .percent,.easy-pie-chart#system-load2 .percent {
            line-height: 128px;

        }
    </style>
@endsection
@section('content')
    <div class="panel panel-headline">
        <div class="panel-heading">
            <h3 class="panel-title">Anasayfa</h3>
            <p class="panel-subtitle">En son giriş : {{date('d.m.Y H:i:s')}}</p>
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-md-4">
                    <div class="metric">
                        <span class="icon"><i class="lnr lnr-dice"></i></span>
                        <p>
                            <span class="number">36</span>
                            <span class="title">TEMEL SORGULAR</span>
                        </p>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="metric">
                        <span class="icon"><i class="fa fa-random"></i></span>
                        <p>
                            <span class="number">12</span>
                            <span class="title">KARMAŞIK SORGULAR</span>
                        </p>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="metric">
                        <span class="icon"><i class="lnr lnr-magic-wand"></i></span>
                        <p>
                            <span class="number">24</span>
                            <span class="title">KONTROL/VERİ TANIMLAMA</span>
                        </p>
                    </div>
                </div>

            </div>
            <div class="row">
                <div class="col-md-4">
                    <!-- REALTIME CHART -->
                    <div class="panel">
                        <div class="panel-heading">
                            <h3 class="panel-title">Temel Sorgular</h3>

                        </div>
                        <div class="panel-body" style="display: block;">
                            <div id="system-load" class="easy-pie-chart" data-percent="18">
                                <span class="percent">18</span>
                                <canvas height="130" width="130"></canvas></div>
                            <ul class="list-unstyled list-justify">
                                <li>Toplam Soru : <span>36</span></li>
                                <li>Çözülen Soru :  <span>6</span></li>
                                <li>Kalan Soru :  <span>30</span></li>

                            </ul>
                        </div>
                    </div>
                    <!-- END REALTIME CHART -->
                </div>

                <div class="col-md-4">
                    <!-- REALTIME CHART -->
                    <div class="panel">
                        <div class="panel-heading">
                            <h3 class="panel-title">Karmaşık Sorgular</h3>

                        </div>
                        <div class="panel-body" style="display: block;">
                            <div id="system-load1" class="easy-pie-chart" data-percent="0">
                                <span class="percent " style="margin-left:30px ">0</span>
                                <canvas height="130" width="130"></canvas></div>
                            <ul class="list-unstyled list-justify">
                                <li>Toplam Soru : <span>12</span></li>
                                <li>Çözülen Soru :  <span>0</span></li>
                                <li>Kalan Soru :  <span>12</span></li>

                            </ul>
                        </div>
                    </div>
                    <!-- END REALTIME CHART -->
                </div>
                <div class="col-md-4">
                    <!-- REALTIME CHART -->
                    <div class="panel">
                        <div class="panel-heading">
                            <h3 class="panel-title">Kontrol/Veri Tanımlama</h3>

                        </div>
                        <div class="panel-body" style="display: block;">
                            <div id="system-load2" class="easy-pie-chart" data-percent="0">
                                <span class="percent " style="margin-left:30px ">0</span>
                                <canvas height="130" width="130"></canvas></div>
                            <ul class="list-unstyled list-justify">
                                <li>Toplam Soru : <span>24</span></li>
                                <li>Çözülen Soru :  <span>0</span></li>
                                <li>Kalan Soru :  <span>24</span></li>

                            </ul>
                        </div>
                    </div>
                    <!-- END REALTIME CHART -->
                </div>
            </div>
        </div>
    </div>
@endsection