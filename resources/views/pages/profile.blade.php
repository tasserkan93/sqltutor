@extends('master.home')
@section('title')

@section('content')
						<div class="clearfix">
							<!-- LEFT COLUMN -->
							<div class="profile-left">
								<!-- PROFILE HEADER -->
								<div class="profile-header">
									<div class="overlay"></div>
									<div class="profile-main">
										<img src="{{asset('assets/img/user-medium.png')}}" class="img-circle" alt="Avatar">
										<h3 class="name">{{session()->get(DFN_USER)['name']}}</h3>
										
									</div>
									<div class="profile-stat">
										<div class="row">
											<div class="col-md-4 stat-item">
												5/36 <span>Temel Sorgular</span>
											</div>
											<div class="col-md-4 stat-item">
												0/12 <span>Karmaşık Sorgular</span>
											</div>
											<div class="col-md-4 stat-item">
												 0/24<span>Kontrol Tanımlama</span>
											</div>
										</div>
									</div>
								</div>
								<!-- END PROFILE HEADER -->
								<!-- PROFILE DETAIL -->
								<div class="profile-detail">
									<div class="profile-info">
										<h4 class="heading">Kişisel Bilgiler</h4>
										<ul class="list-unstyled list-justify">
											<li>Kullanıcı Adı <span>{{session()->get(DFN_USER)['username']}}</span></li>
											<li>Email <span>{{session()->get(DFN_USER)['email']}}</span></li>
											<li>Hesap Türü <span> @if(session()->get(DFN_USER)['userType']==0) Öğrenci @else Öğretmen @endif</span></li>
											
										</ul>
									</div>
									<div class="profile-info">
										
									</div>
									<div class="profile-info">
									
									</div>
									<div class="text-center"><a href="#" class="btn btn-primary">Profili Düzenle</a></div>
								</div>
								<!-- END PROFILE DETAIL -->
							</div>
							<!-- END LEFT COLUMN -->
							<!-- RIGHT COLUMN -->
							<div class="profile-right">
								<h4 class="heading">{{session()->get(DFN_USER)['name']}} Duvar</h4>
								<!-- AWARDS -->
								
								<!-- END AWARDS -->
								<!-- TABBED CONTENT -->
								<div class="custom-tabs-line tabs-line-bottom left-aligned">
									<ul class="nav" role="tablist">
										<li class="active"><a href="#tab-bottom-left1" role="tab" data-toggle="tab">Recent Activity</a></li>
										
									</ul>
								</div>
								<div class="tab-content">
									<div class="tab-pane fade in active" id="tab-bottom-left1">
										<ul class="list-unstyled activity-timeline">
											<li>
												<i class="fa fa-comment activity-icon"></i>
												<p>Select Leveline  başladı <span class="timestamp"></span></p>
											</li>
											<li>
												<i class="fa fa-comment activity-icon"></i>
												<p>Distinct Leveline  başladı <span class="timestamp"></span></p>
											</li>
										</ul>
										
									</div>
									
								</div>
								<!-- END TABBED CONTENT -->
							</div>
							<!-- END RIGHT COLUMN -->
						</div>
			@endsection		