    @extends('master.teach')

    @section('title','Sınıf Oluştur')

    @section('content')
        <div class="panel panel-headline">
            <div class="panel-heading">
                <h3 class="panel-title">Sınıf Oluştur</h3>


            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="metric">
                            <span class="icon"><i class="lnr lnr-construction"></i></span>
                            <p>
                                <span class="number">5</span>
                                <span class="title">SINIFLARIM</span>
                            </p>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="metric">
                            <span class="icon"><i class="fa fa-users"></i></span>
                            <p>
                                <span class="number">10</span>
                                <span class="title">ÖĞRENCİLERİM</span>
                            </p>
                        </div>
                    </div>
                   <div class="col-md-12">
                       <h4> <span class="fa fa-lightbulb-o" style="font-size: 30px;color: #ffc107"></span>
                           Merhaba <strong> {{session()->get(DFN_USER)['name']}}</strong> bu sayfada sanal sınıf oluşturabilir,
                           Sanal sınıfına öğrencilerinizi ekleyebilirsiniz.</h4>

                       <h4> <span class="fa fa-lightbulb-o" style="font-size: 30px;color: #ffc107"></span>
                           Aşağıdaki kutucuğa oluşturacağınız sınıfın adını girip butona tıklayarak sanal sınıfınızı oluşturabilirisiniz.
                       </h4>
                   </div>

                    <div class="col-md-12 text-center" >
                        <hr>
                        <form action="/createclass" method="post">
                            {{csrf_field()}}
                            <h3  for="classname"> Sınıf Adı</h3>
                                <input  name="classname" class="form-control " type="text" style="width: 400px;margin: auto"
                                placeholder="Sınıfı adını giriniz">
                            <input type="submit" class="btn btn-success" value="Oluştur" style="margin: 20px">
                        </form>
                        @if(session('messages'))
                                <div class="alert alert-warning">
                                    {{session('messages')}}
                                </div>
                         
                        @endif

                    </div>
                </div>

            </div>
        </div>
    @endsection