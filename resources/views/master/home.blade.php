<!doctype html>
<html lang="en">

<head>
    <title>@yield('title')</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <!-- VENDOR CSS -->
    <link rel="stylesheet" href="{{asset('assets/vendor/bootstrap/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/vendor/font-awesome/css/font-awesome.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/vendor/linearicons/style.css')}}">
    <link rel="stylesheet" href="{{asset('assets/vendor/chartist/css/chartist-custom.css')}}">
    <!-- MAIN CSS -->
    <link rel="stylesheet" href="{{asset('assets/css/main.css')}}">
    <!-- FOR DEMO PURPOSES ONLY. You should remove this in your project -->
    <link rel="stylesheet" href="{{asset('assets/css/demo.css')}}">
    <!-- GOOGLE FONTS -->
    @yield('link')
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700" rel="stylesheet">
    <!-- ICONS -->
    <link rel="apple-touch-icon" sizes="76x76" href="{{asset('assets/img/apple-icon.png')}}">
    <link rel="icon" type="image/png" sizes="96x96" href="{{asset('assets/img/favicon.png')}}">
    <style>
        .not-active {
            pointer-events: none;
            cursor: default;
            text-decoration: none;
            color: black;
        }
    </style>
</head>
<body class="">
<!-- WRAPPER -->
<div id="wrapper">
    <!-- NAVBAR -->
    <nav class="navbar navbar-default navbar-fixed-top">
        <div class="container-fluid">
            <div class="navbar-btn">
                <button type="button" class="btn-toggle-fullwidth"><i class="lnr lnr-menu"></i></button>
            </div>
            <div class="brand">
                    <a href="/">SQL'ci</a>
            </div>

            <div id="navbar-menu">
                <ul class="nav navbar-nav navbar-right">
                <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i
                                    class="lnr lnr-question-circle"></i> <span>Yardım</span> <i
                                    class="icon-submenu lnr lnr-chevron-down"></i></a>
                        <ul class="dropdown-menu">
                            <li><a href="#">SQL'ci Nedir</a></li>
                            <li><a href="#">Öğretmen SQL'ci</a></li>
                            <li><a href="#">Öğrenci SQL'ci</a></li>
                            
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><img src="{{asset('assets/img/user.png')}}"
                                                                                        class="img-circle" alt="Avatar">
                            <span>{{session()->get(DFN_USER)['name']}}</span> <i class="icon-submenu lnr lnr-chevron-down"></i></a>
                        <ul class="dropdown-menu">
                            <li><a href="/user/profile"><i class="lnr lnr-user"></i> <span>Profilim</span></a></li>
                            <li><a href="/myclasses"><i class="lnr lnr-envelope"></i> <span>Sınıflarım</span></a></li>
                            <li><a href="/logout"><i class="lnr lnr-exit"></i> <span>Çıkış</span></a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <!-- END NAVBAR -->
    <!-- LEFT SIDEBAR -->
    <div id="sidebar-nav" class="sidebar">
        <div class="slimScrollDiv" style="position: relative; overflow: hidden; width: auto; height: 95%;">
            <div class="sidebar-scroll" style="overflow: hidden; width: auto; height: 95%;">
                <nav>
                    <ul class="nav">
                        <li><a href="/"><i class="lnr lnr-home"></i> <span>ANASAYFA</span></a>
                        </li>

                        <li>
                            <a href="#temelsorgu" data-toggle="collapse" class="collapsed"><i
                                        class="lnr lnr-dice"></i> <span>TEMEL SORGULAR</span> <i
                                        class="icon-submenu lnr lnr-chevron-left"></i></a>
                            <div id="temelsorgu" class="collapse ">

                                <ul class="nav">

                                    @foreach(session()->get(DFN_ALL_TITLES) as $titles)
                                        @if($titles['baseTitle']=='TEMEL SORGULAR')
                                            <li><a href="/{{$titles['title']}}/{{$titles['baseTitleId']}}/2"
                                                   @if($titles['baseTitleId']< session()->get('user')['levelId']) style="color: #11b111"
                                                        @elseif($titles['baseTitleId']==session()->get('user')['levelId']) style="color: #c1c704"
                                                    @else
                                                        class="not-active"
                                                        @endif >
                                                    {{$titles['title']}}</a></li>
                                        @endif
                                    @endforeach

                                </ul>
                            </div>
                        </li>

                        <li>
                            <a href="#karmasiksorgu" data-toggle="collapse" class="collapsed"><i
                                        class="fa fa-random"></i> <span>KARMAŞIK SORGULAR</span> <i
                                        class="icon-submenu lnr lnr-chevron-left"></i></a>
                            <div id="karmasiksorgu" class="collapse ">
                                <ul class="nav">
                                    @foreach(session()->get(DFN_ALL_TITLES) as $titles)
                                        @if($titles['baseTitle']=='KARMAŞIK SORGULAR')
                                            <li>
                                            <li><a href="/{{$titles['title']}}/{{$titles['baseTitleId']}}/2"
                                                   @if($titles['baseTitleId']< session()->get('user')['levelId']) style="color: #11b111"
                                                   @elseif($titles['baseTitleId']==session()->get('user')['levelId']) style="color: #c1c704"
                                                   @else
                                                   class="not-active"
                                                        @endif >
                                                    {{$titles['title']}}</a></li>
                                        @endif
                                    @endforeach
                                </ul>
                            </div>
                        </li>

                        <li>
                            <a href="#kontrol" data-toggle="collapse" class="collapsed"><i
                                        class=" lnr lnr-magic-wand"></i> <span>KONTROL/VERİ TANIM.</span> <i
                                        class="icon-submenu lnr lnr-chevron-left"></i></a>
                            <div id="kontrol" class="collapse ">
                                <ul class="nav">
                                    @foreach(session()->get(DFN_ALL_TITLES) as $titles)
                                        @if($titles['baseTitle']=='VERİ TANIMLAMA')
                                            <li><a href="/{{$titles['title']}}/{{$titles['baseTitleId']}}/2"
                                                   @if($titles['baseTitleId']< session()->get('user')['levelId']) style="color: #11b111"
                                                   @elseif($titles['baseTitleId']==session()->get('user')['levelId']) style="color: #c1c704"
                                                   @else
                                                   class="not-active"
                                                        @endif >
                                                    {{$titles['title']}}</a></li>
                                        @endif
                                    @endforeach
                                </ul>
                            </div>
                        </li>

                    </ul>
                </nav>
            </div>
            <div class="slimScrollBar"
                 style="background: rgb(0, 0, 0); width: 7px; position: absolute; top: 0px; opacity: 0.4; display: none; border-radius: 7px; z-index: 99; right: 1px; height: 403.603px;"></div>
            <div class="slimScrollRail"
                 style="width: 7px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51); opacity: 0.2; z-index: 90; right: 1px;"></div>
        </div>
    </div>
    <!-- END LEFT SIDEBAR -->
    <!-- MAIN -->
    <div class="main">
        <!-- MAIN CONTENT -->
        <div class="main-content">
            <div class="container-fluid">
                @yield('content')
            </div>
        </div>
        <!-- END MAIN CONTENT -->
    </div>
    <!-- END MAIN -->
    <div class="clearfix"></div>
    <footer>
        <div class="container-fluid">
            <p class="copyright">© 2017 <a href="https://www.themeineed.com" target="_blank">Theme I Need</a>. All
                Rights Reserved.</p>
        </div>
    </footer>
</div>
<!-- END WRAPPER -->
<!-- Javascript -->
<script src="{{asset('assets/vendor/jquery/jquery.min.js')}}"></script>
<script src="{{asset('assets/vendor/bootstrap/js/bootstrap.min.js')}}"></script>
<script src="{{asset('assets/vendor/jquery-slimscroll/jquery.slimscroll.min.js')}}"></script>

@yield('script')
<script src="{{asset('assets/vendor/jquery.easy-pie-chart/jquery.easypiechart.min.js')}}"></script>
<script src="{{asset('assets/vendor/chartist/js/chartist.min.js')}}"></script>
<script src="{{asset('assets/scripts/klorofil-common.js')}}"></script>

<script>
    $(function() {


        $('#system-load').easyPieChart({
            size: 130,
            barColor: function(percent) {
                return "#459447";
            },
            trackColor: '#eee',
            scaleColor: false,
            lineWidth: 5,
            lineCap: "square",
            animate: 800
        });

        $('#system-load1').easyPieChart({
            size: 130,
            barColor: function(percent) {
                return "#f38c0a";
            },
            trackColor: '#eee',
            scaleColor: false,
            lineWidth: 5,
            lineCap: "square",
            animate: 800
        });

        $('#system-load2').easyPieChart({
            size: 130,
            barColor: function(percent) {
                return "#d61211";
            },
            trackColor: '#eee',
            scaleColor: false,
            lineWidth: 5,
            lineCap: "square",
            animate: 800
        });


    });
</script>

</body>
